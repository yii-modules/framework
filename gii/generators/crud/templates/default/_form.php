<?php
$enableDatepicker = false;
$hasFk = false;

$hasActive = $this->hasColumn($this->tableSchema->columns, 'active');
$hasName = $this->hasColumn($this->tableSchema->columns, 'name');
$hasUrl = $this->hasColumn($this->tableSchema->columns, 'url');

$hasImages = $this->hasImages($this->tableSchema->columns);
$multiPartForm = $hasImages ? "\n'htmlOptions'=>array('enctype'=>'multipart/form-data')," : "";
$enableJavascriptValidation = $hasImages ? 'false' : 'true';
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="form form-horizontal well well-small">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
        'enableClientValidation'=>{$enableJavascriptValidation},
	'enableAjaxValidation'=>{$enableJavascriptValidation},
        'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false,
        ),$multiPartForm
)); ?>\n"; ?>

<?php if ( $hasActive ): ?>
<?php
echo "
        <?php
        if (\$model->isNewRecord)
                \$model->active = 1;
        ?>
";
?>

        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$hasActive)."; ?>\n"; ?>
                <div class='controls'>
                        <div class='onoffswitch'>
                                <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$hasActive)."; ?>\n"; ?>
                                <label class='onoffswitch-label' for='switcher_<?php echo $this->modelClass.'_'.$hasActive->name; ?>'>
                                        <div class='onoffswitch-inner'>
                                                <div class='onoffswitch-active'><div class='onoffswitch-switch'><?php echo "<?php echo Yii::t('admin','Да'); ?>"; ?></div></div>
                                                <div class='onoffswitch-inactive'><div class='onoffswitch-switch'><?php echo "<?php echo Yii::t('admin','Нет'); ?>"; ?></div></div>
                                        </div>
                                </label>
                        </div>
                        <?php echo "<?php echo \$form->error(\$model,'{$hasActive->name}'); ?>\n"; ?>
                </div>
        </div>

<?php endif ?>
<?php if ( $hasName ): ?>
        <div class='control-group'>
                <?php echo "<?php echo \$form->labelEx(\$model,'name', array('class'=>'control-label')); ?>\n"; ?>
                <div class='controls'>
                        <?php echo "<?php echo \$form->textField(\$model,'name', array('class'=>'span25', 'autofocus'=>true)); ?>\n"; ?>
                        <?php echo "<?php echo \$form->error(\$model,'name'); ?>\n"; ?>
                </div>
        </div>

<?php endif ?>
<?php if ( $hasUrl ): ?>
        <div class='control-group'>
                <?php echo "<?php echo \$form->labelEx(\$model,'url', array('class'=>'control-label')); ?>\n"; ?>
                <div class='controls'>
                        <?php echo "<?php echo \$form->textField(\$model,'url', array('class'=>'span25')); ?>\n"; ?>
                        <i class='icon-question-sign help-popover' 
                                data-original-title='<?php echo Yii::t("help","URL ссылка") ?>' 
                                onmouseover="$(this).popover('show')"
                                data-content='<?php echo Yii::t("help",'Оставьте поле пустым для автоматической генерации.<br/><br/>
                                Либо введите название.<br/>Например:
                                <span class="label">contact</span> или <span class="label">about</span><br/><br/>
                                Ваша ссылка будет выглядеть так:<br/>
                                <pre>http://site.com/<span class="label">contact</span>/</pre>' ) ?>'>
                        </i>
                        <?php echo "<?php echo \$form->error(\$model,'url'); ?>\n"; ?>
                </div>
        </div>

<?php endif ?>

<?php
foreach($this->tableSchema->columns as $column)
{
	if($column->autoIncrement OR in_array($column->name, array('active', 'create_time', 'update_time', 'sorter', 'name', 'url')))
		continue;
?>
<?php if(stripos($column->name, 'price') !== false): ?>
        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                <div class='controls'>
                        <div class='input-append'>
                                <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                                <span class='add-on'>€</span>
                        </div>

                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
                </div>
        </div>

<?php elseif( (stripos($column->name, 'date') !== false) OR (stripos($column->dbType, 'date') !== false) ): ?>
<?php $enableDatepicker = true; ?>
        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                <div class='controls'>
                        <div class='input-append'>
                                <?php echo "<?php echo \$form->textField(\$model,'{$column->name}', array('class'=>'span4')); ?>\n"; ?>
                                <span class='add-on'><i class='icon-calendar'></i></span>
                        </div>

                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
                </div>
        </div>

<?php elseif($this->isImage($column->name)): ?>
        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                <div class='controls'>
                        <?php echo "<?php echo \$form->fileField(\$model,'{$column->name}'); ?>\n"; ?>
                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>

                        <?php echo "<?php if(! \$model->isNewRecord AND is_file(\$model->getUploadDir() . '/big/' . \$model->{$column->name})): ?>
                                <br>
                                <?php echo CHtml::image(\$model->getImageUrl('big', '{$column->name}'), \$model->name); ?>
                        <?php endif ?>"; ?>

                </div>
        </div>

<?php elseif(stripos($column->dbType, 'tinyint') !== false): ?>
        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                <div class='controls'>
                        <div class='onoffswitch'>
                                <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                                <label class='onoffswitch-label' for='switcher_<?php echo $this->modelClass.'_'.$column->name; ?>'>
                                        <div class='onoffswitch-inner'>
                                                <div class='onoffswitch-active'><div class='onoffswitch-switch'><?php echo "<?php echo Yii::t('admin','Да'); ?>"; ?></div></div>
                                                <div class='onoffswitch-inactive'><div class='onoffswitch-switch'><?php echo "<?php echo Yii::t('admin','Нет'); ?>"; ?></div></div>
                                        </div>
                                </label>
                        </div>
                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
                </div>
        </div>

<?php else: ?>
        <div class='control-group'>
                <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                <div class='controls'>
                        <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
<?php if ( $column->isForeignKey ): ?>
                        
<?php
$hasFk = true;
$fkContoller = lcfirst($this->fkToClassName($column->name));
echo "
                        <?php echo CHtml::link(
                                Yii::t('admin','Добавить'),
                                array('/{$fkContoller}/create'),
                                array('class'=>'fancybox fancybox.ajax btn btn-small')
                        ); ?>
";
?>
<?php endif ?>
                </div>
        </div>

<?php endif ?>
<?php
}
?>
        <?php echo "<?php echo CHtml::htmlButton(
                \$model->isNewRecord ? 
               '<i class=\"icon-plus-sign icon-white\"></i> '.Yii::t('admin','Создать') : 
               '<i class=\"icon-ok icon-white\"></i> '.Yii::t('admin','Сохранить'),
                array(
                        'class'=>'btn btn-info controls',
                        'type'=>'submit',
                )
        ); ?>\n"; ?>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div>

<?php
if ($enableDatepicker)
echo "<?php \$this->widget('ext.BootstrapDatepicker.BootstrapDatepicker'); ?>";
?>

<?php if ( $hasFk ): ?>
<?php echo '<?php $this->widget("ext.Fancybox.Fancybox") ?>'; ?>

<script>
$(function(){
        $(document).on('submit', '.fancybox-inner form', function(){
                event.preventDefault();
                var form = $('.fancybox-inner form');
                var getJsonDataUrl = form.attr('action').replace(/create$/, 'getJsonData');

                var tmp = form.attr('action').split('/');
                tmp.pop('create');
                var tmpSelector = tmp[tmp.length - 1];
                var targetSelector = $('#dropDown' + tmpSelector.charAt(0).toUpperCase() + tmpSelector.slice(1));

                $.post(form.attr('action'), form.serialize())
                        .success(function(id){
                                $.get(getJsonDataUrl)
                                        .success(function(data){
                                                targetSelector.html('<option></option>');
                                                $.each(JSON.parse(data), function (index, value) {
                                                        targetSelector.append('<option value="'+index+'">'+value+'</option>');
                                                });

                                                targetSelector.find('>option:selected').attr('selected', null);
                                                targetSelector.find('>option[value='+id+']').attr('selected', true);
                                        });
                        });

                $.fancybox.close();
        });
});
</script>

<?php endif ?>
