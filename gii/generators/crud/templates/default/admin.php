<?php
$hasActive = $this->hasColumn($this->tableSchema->columns, 'active');
$hasName = $this->hasColumn($this->tableSchema->columns, 'name');
$hasUrl = $this->hasColumn($this->tableSchema->columns, 'url');
$hasSorter = $this->hasColumn($this->tableSchema->columns, 'sorter');
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo '<?php $this->breadcrumbs = array(
        Yii::t("'.$this->tlabel.'", "'.$this->adminName.'"),
); ?>';
?>


<?php
echo '<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>';
?>

<h2><?php echo '<?php echo Yii::t("'.$this->tlabel.'","'.$this->adminName.'"); ?>'; ?></h2>

<?php echo "<?php echo CHtml::link(
       '<i class=\"icon-plus-sign icon-white\"></i> '. Yii::t('admin','Создать'), 
        array('create'),
        array('class'=>'btn btn-info')
); ?>"; ?>


<?php echo '<?php $form=$this->beginWidget("CActiveForm"); ?>'; ?>


<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
        'ajaxUpdate'=>false,
        'template'=>'{items}{pager}{summary}',
	'filter'=>$model,
	'columns'=>array(
                array(
                        'header'=>'№',
                        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        )
                ),
<?php if ( $hasName ): ?>
                array(
                        'name'=>'name',
                        'value'=>'CHtml::link($data->name, array("update", "id"=>$data->id))',
                        'type'=>'raw',
                ),
<?php endif ?>
<?php if ( $hasUrl ): ?>
                'url',
<?php endif ?>
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->autoIncrement OR $this->isText($column) OR in_array($column->name, array('active', 'create_time', 'update_time', 'sorter', 'name', 'url', 'body', 'description')))
		continue;

	if(++$count==7)
		echo "\t\t/*\n";

        if ($this->isImage($column->name)) 
        {
?>
                array(
                        'name'=>'<?php echo $column->name; ?>',
                        'value'=>'$data-><?php echo $column->name; ?> ? CHtml::image($data->getImageUrl("for_grid", "<?php echo $column->name; ?>")) : ""',
                        'filter'=>false,
                        'type'=>'raw',
                        'htmlOptions'=>array(
                                'width'=>'55',
                                'class'=>'centered'
                        )
                ),
<?php
        }
        elseif ($column->isForeignKey)
        {
?>
                array(
                        'name'=>'<?php echo $column->name; ?>',
                        'filter'=>CHtml::listData(<?php echo $this->fkToClassName($column->name); ?>::model()->findAll(), 'id', 'name'),
                        'value'=>'CHtml::value($data-><?php echo lcfirst($this->fkToClassName($column->name)); ?>, "name")',
                ),
<?php
        }
        else
                echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
<?php if ( $hasSorter ): ?>
                array(
                        'name'=>'sorter',
                        'type'=>'raw',
                        'value'=>'CHtml::textField("sortOrder[$data->id]",$data->sorter,array("class"=>"sorter-field", "maxlength"=>5))',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        )
                ),
<?php endif ?>
<?php if ( $hasActive ): ?>
                array(
                        'name'=>'active',
                        'filter'=>array(1=>Yii::t("admin","Да"), 0=>Yii::t("admin","Нет")),
                        'value'=>'MF::attributeToggler($data, "active", array(Yii::t("admin","Да"),Yii::t("admin","Нет")))',
                        'type'=>'raw',
                        'htmlOptions'=>array(
                                'width'=>'55',
                                'class'=>'centered',
                        )
                ),
<?php endif ?>
                array(
                        'id'=>'autoId',
                        'class'=>'CCheckBoxColumn',
                        'selectableRows'=>2,
                ),
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update} {delete}',
                        'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100,200=>200),array(
                                'onchange'=>"$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid',{ data:{pageSize: $(this).val() }})",
                                'style'=>'width:50px'
                        )),
		),
	),
        'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>


<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid');
}
</script>

<?php echo '<?php echo CHtml::ajaxSubmitButton("",array(), array(),array("style"=>"visibility:hidden;")); ?>'; ?>

<?php if($hasSorter): ?>
<div class='row'>
        <div class='span12 pull-right'>
                <?php echo '<?php echo CHtml::ajaxSubmitButton(
                        Yii::t("admin", "Удалить выбраное"),
                        array("deleteSelected"), 
                        array("success"=>"reloadGrid"),
                        array(
                                "class"=>"btn btn-small pull-right", 
                                "confirm"=>Yii::t("admin","Удалить выбраные элементы ?"),
                        )
                ); ?>

                <?php echo CHtml::ajaxSubmitButton(
                        Yii::t("admin","Обновить порядок"),
                        array("sorterUpdate"), 
                        array("success"=>"reloadGrid"),
                        array("class"=>"btn btn-small pull-right")
                ); ?>'; ?>

        </div>
</div>
<?php else: ?>
<?php echo '<?php echo CHtml::ajaxSubmitButton(
        Yii::t("admin", "Удалить выбраное"),
        array("deleteSelected"), 
        array("success"=>"reloadGrid"),
        array(
                "class"=>"btn btn-small pull-right", 
                "confirm"=>Yii::t("admin","Удалить выбраные элементы ?"),
        )
); ?>'; ?>
<?php endif ?>

<?php echo '<?php $this->endWidget(); ?>'; ?>

