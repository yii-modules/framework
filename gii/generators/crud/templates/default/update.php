<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php if ( ! Yii::app()->request->isAjaxRequest): ?>"; ?>

<?php
echo '<?php $this->breadcrumbs = array(
        Yii::t("'.$this->tlabel.'","'.$this->adminName.'")=>array("admin"),
        Yii::t("admin","Редактирование"),
); ?>';
?>


<h2><?php echo '<?php echo Yii::t("admin","Редактирование")." - ".$model->name; ?>'; ?></h2>
<?php echo "<?php endif; ?>"; ?>

<?php echo "<?php echo \$this->renderPartial('_form', compact('model')); ?>"; ?>
