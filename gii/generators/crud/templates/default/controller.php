<?php
$hasImages = $this->hasImages($this->tableSchema->columns);
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends AdminDefaultController
{
        public $modelName = '<?php echo $this->modelClass; ?>';
        public $layout = '//layouts/back';

        public $noPages = array('index','view', 'redirect'=>array('admin'));
        public $createRedirect = array('admin');
        public $updateRedirect = 'goBack';

<?php if ( $hasImages ): ?>
        /**
         * actionCreate 
         */
        public function actionCreate()
        {
                $model = new <?php echo $this->modelClass; ?>;

                if (isset($_POST['<?php echo $this->modelClass; ?>'])) 
                {
                        $model->attributes = $_POST['<?php echo $this->modelClass; ?>'];

<?php foreach($hasImages as $imageFieldName): ?>
                        $file_<?php echo $imageFieldName; ?> = CUploadedFile::getInstance($model, '<?php echo $imageFieldName; ?>');
                        $model-><?php echo $imageFieldName; ?> = $file_<?php echo $imageFieldName; ?>;

<?php endforeach ?>

                        if ($model->validate()) 
                        {
<?php foreach($hasImages as $imageFieldName): ?>
                                if ( $file_<?php echo $imageFieldName; ?> ) 
                                {
                                        $model-><?php echo $imageFieldName; ?> = $model->makeImageName($model-><?php echo $imageFieldName; ?>);
                                        $model->saveImage($file_<?php echo $imageFieldName; ?>, $model-><?php echo $imageFieldName; ?>);
                                }

<?php endforeach ?>
                                $model->save(false);

                                if ( Yii::app()->request->isAjaxRequest ) 
                                {
                                        echo $model->id;
                                        Yii::app()->end();
                                }
                                else
                                {
                                        $this->redirect(array('admin'));
                                }
                        }
                }

                if ( Yii::app()->request->isAjaxRequest ) 
                        $this->renderPartial('create', compact('model'), false, true);
                else 
                        $this->render('create', compact('model'));
        }


        /**
         * actionUpdate 
         * 
         * @param int $id 
         */
        public function actionUpdate($id)
        {
		$model = $this->loadModel($id);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];

<?php foreach($hasImages as $imageFieldName): ?>
                        $file_<?php echo $imageFieldName; ?> = CUploadedFile::getInstance($model, '<?php echo $imageFieldName; ?>');
                        if ( $file_<?php echo $imageFieldName; ?> ) 
                        {
                                $old_<?php echo $imageFieldName; ?> = $model-><?php echo $imageFieldName; ?>;
                                $model-><?php echo $imageFieldName; ?> = $file_<?php echo $imageFieldName; ?>;
                        }

<?php endforeach ?>

			if($model->validate())
                        {
<?php foreach($hasImages as $imageFieldName): ?>
                                if ( $file_<?php echo $imageFieldName; ?> ) 
                                {
                                        $model-><?php echo $imageFieldName; ?> = $model->makeImageName($model-><?php echo $imageFieldName; ?>);
                                        $model->saveImage($file_<?php echo $imageFieldName; ?>, $model-><?php echo $imageFieldName; ?>);

                                        $model->deleteImages(array('$old_<?php echo $imageFieldName; ?>'));
                                }

<?php endforeach ?>
                                $model->save(false);

                                if ( Yii::app()->request->isAjaxRequest ) 
                                {
                                        echo $model->id;
                                        Yii::app()->end();
                                }
                                else
                                {
                                        $this->redirect(array('admin'));
                                }
                        }
		}

                if ( Yii::app()->request->isAjaxRequest ) 
                        $this->renderPartial('update', compact('model'), false, true);
                else 
                        $this->render('update', compact('model'));
        }

<?php endif ?>
}
