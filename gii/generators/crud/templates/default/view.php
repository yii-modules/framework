<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo '<?php $this->breadcrumbs = array(
        "Управление"=>array("admin"),
        "Детали"
); ?>';
?>


<h2>
        <?php echo '<?php echo $model->name; ?>'; ?>

        <?php echo '<?php echo CHtml::link(
                Yii::t("admin","К управлению"),
                array("admin"),
                array("class"=>"btn btn-small pull-right")
        ); ?>'; ?>

        <?php echo '<?php echo CHtml::link(
                Yii::t("admin","Редактировать"),
                array("update", "id"=>$model->id),
                array("class"=>"btn btn-small pull-right")
        ); ?>'; ?>
</h2>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
        'htmlOptions'=>array('class'=>'table table-condensed table-striped table-hover table-bordered table-side'),
)); ?>
