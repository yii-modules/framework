<?php
$withCriteria = array();

$hasImages = $this->hasImages($columns);
$tPrefix = array_key_exists('sorter', $labels) ? $modelClass : 't';
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?php echo "<?php\n"; ?>

/**
 * This is the model class for table "<?php echo $tableName; ?>".
 *
 * The followings are the available columns in table '<?php echo $tableName; ?>':
<?php foreach($columns as $column): ?>
 * @property <?php echo $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * The followings are the available model relations:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
	if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = $matches[2];

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                    $withCriteria[] = $name;
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
	}
    ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?php echo $modelClass; ?> extends <?php echo $this->baseClass."\n"; ?>
{
<?php if($hasImages): ?>
        // Куда будут сохраняться картинки (субдиректории)
        public $thumbDirs = array('for_grid', 'big');


<?php endif ?>
<?php foreach($labels as $key => $val): ?>
<?php if (stripos($key, '_time')): ?>
        public $hf_<?php echo $key; ?>;
<?php endif ?>
<?php endforeach ?>

<?php if(array_key_exists('sorter', $labels)): ?>
        public function defaultScope()
        {
                return array(
                        'alias'=>'<?php echo $modelClass; ?>',
                        'order'=>'<?php echo $modelClass; ?>.sorter ASC, <?php echo $modelClass; ?>.id DESC',
                );
        }

<?php endif ?>
<?php if(array_key_exists('active', $labels)): ?>
        public function scopes()
        {
                return array(
                        'active'=>array(
                                'alias'=>'<?php echo $modelClass; ?>',
                                'condition'=>'<?php echo $modelClass; ?>.active = 1',
                        ),
                );
        }
<?php endif ?>

        //=========== Default stuff ===========
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
<?php if($connectionId!='db'):?>

	public function getDbConnection()
	{
		return Yii::app()-><?php echo $connectionId ?>;
	}
<?php endif?>

	public function tableName()
	{
		return '<?php echo $tableName; ?>';
	}

	public function rules()
	{
		return array(
<?php foreach($rules as $rule): ?>
			<?php echo $rule.",\n"; ?>
<?php endforeach; ?>
			array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
<?php foreach($relations as $name=>$relation): ?>
			<?php echo "'$name' => $relation,\n"; ?>
<?php endforeach; ?>
		);
	}

	public function attributeLabels()
	{
		return array(
<?php foreach($labels as $name=>$label): ?>
<?php if ($name == 'id') { continue; } ?>
<?php $translatedLabel = $this->getRussianLabel($label); ?>
			<?php echo "'$name' => Yii::t('$this->tlabel','$translatedLabel'),\n"; ?>
<?php endforeach; ?>
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
<?php if ( $withCriteria !== array() ): ?>

                $criteria->with = array('<?php echo implode("', '", $withCriteria); ?>');
<?php endif ?>

<?php
foreach($columns as $name=>$column)
{
        if ( $this->isImage($name) OR $this->isText($column) ) 
                continue;
	if($column->type==='string')
	{
		echo "\t\t\$criteria->compare('$tPrefix.$name',\$this->$name,true);\n";
	}
	else
	{
		echo "\t\t\$criteria->compare('$tPrefix.$name',\$this->$name);\n";
	}
}
?>

		return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
<?php if(array_key_exists('sorter', $labels)): ?>
                        'sort'=>array(
                                'defaultOrder'=>'<?php echo $tPrefix; ?>.sorter ASC, <?php echo $tPrefix; ?>.id DESC',
                        ),
<?php else: ?>
                        'sort'=>array(
                                'defaultOrder'=>'<?php echo $tPrefix; ?>.id DESC',
                        ),
<?php endif ?>
                        'pagination'=>array(
                                'pageSize'=>Yii::app()->user->getState('pageSize',20), 
                        ),
		));
	}
        //----------- Default stuff -----------
<?php if($hasImages): ?>
<?php if ( $this->customImagePath ): ?>

        /**
         * getUploadDir 
         *
         * + Создаёт директории, если их нет
         * 
         * @return string
         */
        public function getUploadDir()
        {
                return Yii::getPathOfAlias('webroot.images.<?php echo $tableName; ?>');
        }

        /**
         * getImageUrl 
         * 
         * @param string|null $dir 
         * @param string $attr 
         * @return string
         */
        public function getImageUrl($dir = 'big', $attr = 'image')
        {
                if ( $dir ) 
                        return Yii::app()->baseUrl."/images/{$this->tableName()}/{$dir}/".$this->{$attr};
                else
                        return Yii::app()->baseUrl."/images/{$this->tableName()}/".$this->{$attr};
        }

<?php endif ?>

        /**
         * saveImage 
         * 
         * @param CUploadedFile $file 
         * @param string $imageName 
         */
        public function saveImage($file, $imageName)
        {
                if ( ! $file ) 
                        return false;

                $uploadDir = $this->getUploadDir();

                $this->_prepareUploadDir($uploadDir);

                $ih = new CImageHandler;
                $ih->load($file->tempName)
                        ->thumb('500','500')
                        ->save($uploadDir.'/big/'.$imageName)
                        ->thumb('50','50')
                        ->save($uploadDir.'/for_grid/'.$imageName);
        }



<?php endif ?>

        //=========== Lists ===========

        //----------- Lists -----------

<?php if(array_key_exists('url', $labels)): ?>

        /**
         * Если админ не заполнил "URL ссылка", то она генерируется латиницей из названия
         * А если заполнил, то очищается от кириллических и других не подходящих символов
         */
        protected function beforeValidate()
        {
                if (! $this->url)
                        $this->url = MF::translit(mb_strtolower($this->name,'UTF-8'));
                else
                        $this->url = MF::translit(mb_strtolower($this->url,'UTF-8'));

                return parent::beforeValidate();
        }

<?php endif ?>
<?php if (array_key_exists('create_time', $labels) OR array_key_exists('update_time', $labels)): ?>

        /**
         * beforeSave 
         * 
         * Timestamps и т.д.
         */
        protected function beforeSave()
        {
                if ($this->isNewRecord) 
                        $this->create_time = time();

                $this->update_time = time();

                return parent::beforeSave();
        }

<?php endif ?>
<?php
// Проверяем, надо ли создавать afterFind() function
$haveTimeFields = false;
foreach ($labels as $key => $val) 
{
        if (stripos($key, '_time'))
        {
                $haveTimeFields = true;
                break;
        }
}
?>
<?php if($haveTimeFields): ?>

        /**
         * afterFind 
         * 
         * Для полей с датами в int создаём human format (hf_) в нормальной форме
         */
        protected function afterFind()
        {
<?php foreach($labels as $key => $val): ?>
<?php if (stripos($key, '_time')): ?>
                $this->hf_<?php echo $key; ?> = MF::formatDate($this-><?php echo $key; ?>);
<?php endif ?>
<?php endforeach ?>

                parent::afterFind();
        }

<?php endif ?>
<?php if ($hasImages): ?>
<?php $toDelete = array(); ?>

        /**
         * afterDelete 
         * 
         * Удаляем картинки
         */
        protected function afterDelete()
        {
<?php foreach($hasImages as $imageName): ?>
<?php $toDelete[] = '$this->'.$imageName; ?>
<?php endforeach ?>
                $this->deleteImages(array(<?php echo implode(", ", $toDelete); ?>));
                parent::afterDelete();
        }

<?php endif ?>
}
